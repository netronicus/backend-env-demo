COMO COMENZAR
=============

Primero que todo, es necesario instalar [Nodejs](https://nodejs.org/), elija el método indicado para su Sistema Operativo.

## Dependencies

Una vez instalado Nodejs, es necesario instalar las dependencias. Ejecute la siguiguiente línea para instalarlas:

        npm install

## Ejecutando pruebas unitarias

Si desea correr las pruebas unitarias existentes, puede ejecutar el siguiente comando:

        npm run test

Las pruebas unitarias existentes tienen activado el modo depuración, debido a que facilita la identificación de las cadenas, puede ser desactivado de la siguiente manera del archivo test/mutant.test.js

        Cambiando esto:
        var mutant = new Mutant(["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"],true);
        por esto:
        var mutant = new Mutant(["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"],false);

## Como utilizar la aplicación

Para correr la aplicación, debes ejecutar el siguiente comando:

        npm start

Si deseas detener la aplicación, puedes presionar ***Ctrl + C*** para detener la ejecución en la línea de comandos.

La aplicación debe recibir como parámetro un array de Strings que representan cada fila de una tabla
de (NxN) con la secuencia del ADN. Las letras de los Strings solo pueden ser: (A,T,C,G), las
cuales representa cada base nitrogenada del ADN.

Sin mutación:

        A T G C G A
        C A G T G C
        T T A T T T
        A G A C G G
        G C G T C A
        T C A C T G

Con mutación:

        A T G C G A
        C A G T G C
        T T A T G T
        A G A A G G
        C C C C T A
        T C A C T G

La aplicación cuenta con dos puntos de acceso:

        POST → /mutation
        Entrada:
        {"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]}
        Salida:
        Status 200 si tiene mutación
        Status 403 si no tiene mutación
        Status 400 Si el formato de la petición es incorrecto

        GET → /stats
        Salida:
        {"count_mutations":40, "count_no_mutation":100: "ratio":0.4}

La aplicación en producción se encuentra montada en [Google App Engine](https://cloud.google.com/appengine) en el siguiente enlace: [https://backend-env-demo.appspot.com](https://backend-env-demo.appspot.com), también cuenta con integración a una base de datos NoSQL MongoDB Hosteada en [MongoDB Atlas](https://www.mongodb.com/cloud/atlas/mongodb-google-cloud), si ejecutas la aplicación en modo local, podrás ingresar mediante tu explorador en la siguiente ruta:

        http://localhost:8080 + punto de acceso