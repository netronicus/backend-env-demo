class Mutant{
    constructor(dna = [],debug = false){
        this.valid = true;
        this.dna = this.parseMatrix(dna);
        this.debug = debug;
    }
    isValid(){
        return this.valid;
    }
    isMutant(){
        for (let y = 0; y < this.dna.length; y++) {
            const string = this.dna[y];
            for (let x = 0; x < string.length; x++) {
                if(
                    this.getDiagonalRight(y,x) ||
                    this.getDiagonalLeft(y,x) ||
                    this.getHorizontal(y,x) ||
                    this.getVertical(y,x)
                )
                    return true;
            }            
        }
        return false;
    }
    parseMatrix(dna = []){
        var matrix = [];
        for (let x = 0; x < dna.length; x++) {
            const dnaString = dna[x].split('');
            this.validatebase(dnaString);
            matrix.push(dnaString);
        }
        return matrix;
    }
    validatebase(dnaString){
        const bases = ["A","T","C","G"];
        dnaString.forEach(element => {
            if(!bases.includes(element)){
                this.valid = false;
            }
        });
    }
    getDiagonalRight(startY,startX){
        var found = [];
        var x = startX;
        for (let y = startY; y < this.dna[startY].length; y++) {
            const baseN = this.dna[y][x];
            if(found.length == 0){
                found.push(baseN);
            }else if(found[found.length-1] == baseN){
                found.push(baseN);
            }else{
                if(this.debug)console.log('found:',found);
                return found.length >= 4 ? true : false;
            }
            x++;
        }
        if(this.debug)console.log('found:',found);
        return found.length >= 4 ? true : false;
    }
    getDiagonalLeft(startY,startX){
        var found = [];
        var x = startX;
        for (let y = startY; y >= 0; y--) {
            const baseN = this.dna[y][x];
            if(found.length == 0){
                found.push(baseN);
            }else if(found[found.length-1] == baseN){
                found.push(baseN);
            }else{
                if(this.debug)console.log('found:',found);
                return found.length >= 4 ? true : false;
            }
            x++;
        }
        if(this.debug)console.log('found:',found);
        return found.length >= 4 ? true : false;
    }
    getHorizontal(startY,startX){
        var found = [];
        for (let x = startX; x < this.dna[startY].length; x++) {
            const baseN = this.dna[startY][x];
            if(found.length == 0){
                found.push(baseN);
            }else if(found[found.length-1] == baseN){
                found.push(baseN);
            }else{
                if(this.debug)console.log('found:',found);
                return found.length >= 4 ? true : false;
            }
        }
        if(this.debug)console.log('found:',found);
        return found.length >= 4 ? true : false;
    }
    getVertical(startY,startX){
        var found = [];
        for (let y = startY; y < this.dna.length; y++) {
            const baseN = this.dna[y][startX];
            if(found.length == 0){
                found.push(baseN);
            }else if(found[found.length-1] == baseN){
                found.push(baseN);
            }else{
                if(this.debug)console.log('found:',found);
                return found.length >= 4 ? true : false;
            }
        }
        if(this.debug)console.log('found:',found);
        return found.length >= 4 ? true : false;
    }
}

module.exports = Mutant;