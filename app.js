// index.js
const express = require('express');
const bodyParser = require('body-parser');
const Mutant = require('./lib/mutant');
const mongodb = require('mongodb');
const nconf = require('nconf');

nconf.argv().env().file('keys.json');

const user = nconf.get('mongoUser');
const pass = nconf.get('mongoPass');
const host = nconf.get('mongoHost');
const port = nconf.get('mongoPort');

let uri = `mongodb+srv://${user}:${pass}@${host}`;
if (nconf.get('mongoDatabase')) {
  uri = `${uri}/${nconf.get('mongoDatabase')}?retryWrites=true&w=majority`;
}
//console.log(uri);
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

mongodb.MongoClient.connect(uri, (err, db) => {
  if (err) {
    throw err;
  }

  app.post('/mutation', (req, res) => {
      if(req.body.dna){
        var dna = req.body.dna;
        var mutant = new Mutant(dna);
        if(mutant.isValid()){
          let isMutant = mutant.isMutant();
          mongodb.MongoClient.connect(uri, { useNewUrlParser: true,useUnifiedTopology: true }, (err, client) => {
            if (err) {
              throw err;
            }
      
            const db = client.db(nconf.get("mongoDatabase"));
            const collection = db.collection('mutants');
            let key = {
              dna: dna.join('')
            };
            let value = {
              $set: { mutant: isMutant }
            };
            collection.updateOne(key, value, { upsert: true });
          });
          if(mutant.isMutant()){
            res.sendStatus(200);
          }else{
            res.sendStatus(403);
          }
        }else{
          res.sendStatus(400);
        }
      }else{
        res.sendStatus(400);
      }
  });
  app.get('/stats', (req, res) => {
    mongodb.MongoClient.connect(uri, { useNewUrlParser: true,useUnifiedTopology: true }, (err, client) => {
      if (err) {
        throw err;
      }

      const db = client.db(nconf.get("mongoDatabase"));
      const collection = db.collection('mutants');
      collection.find({ mutant: true }).count().then((count_mutations)=>{
        collection.find({ mutant: false }).count().then((count_no_mutation)=>{
          let response = {"count_mutations":count_mutations, "count_no_mutation":count_no_mutation, "ratio": count_mutations == 0 && count_no_mutation == 0 ? 0 : count_no_mutation!=0 ? (count_mutations/count_no_mutation): 1};
          res.json(response);
        });
      });
    });
  });
});

module.exports = app;