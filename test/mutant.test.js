const assert = require('assert');
const expect = require('chai').expect;
const should = require('chai').should();
const Mutant = require("../lib/mutant");
describe('Mutant',() => {
    describe('#parseMatrix()',()=>{
        it('Es una Matriz',()=>{
            var mutant = new Mutant();
            var matrix = mutant.parseMatrix([
                "ATGCGA",
                "CAGTGC",
                "TTATTT",
                "AGACGG",
                "GCGTCA",
                "TCACTG"]);
            expect(matrix).to.be.instanceOf(Array);
            expect(matrix.length).to.not.be.equal(0);
            matrix.forEach(array=>{
                expect(array).to.be.instanceOf(Array);
            })
        });
        it('No es una Matriz',()=>{
            var mutant = new Mutant();
            var matrix = mutant.parseMatrix();
            expect(matrix).to.be.instanceOf(Array);
            expect(matrix.length).to.be.equal(0);
        });
    });
    describe('#isValid()', () => {
        it('Bases válidas', () => {
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTGC",
                "TTATTT",
                "AGACGG",
                "GCGTCA",
                "TCACTG"]);
            expect(mutant.isValid()).to.true;
        });
        it('Bases NO válidas', () => {
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTGC",
                "TTRTTT",
                "AGACGG",
                "GCGTCA",
                "TCACTG"]);
            expect(mutant.isValid()).to.false;
        });
    });
    describe('#validatebase()',()=>{
        it('Cadena válida',()=>{
            var mutant = new Mutant();
            mutant.validatebase(["A","T","C","G"]);
            expect(mutant.valid).to.be.true;
        });
        it('Cadena NO válida',()=>{
            var mutant = new Mutant();
            mutant.validatebase(["A","B","C","D"]);
            expect(mutant.valid).to.be.false;
        });
    });
    describe('#getDiagonalRight()',()=>{
        it('Diagonal derecha encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTCC",
                "TTACTT",
                "AGCAGG",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.getDiagonalRight(0,0)).to.be.true;
        });
        it('Diagonal derecha no encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTCC",
                "TTRCTT",
                "AGACGC",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.getDiagonalRight(5,5)).to.be.false;
        });
    });
    describe('#getDiagonalLeft()',()=>{
        it('Diagonal izquierda encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTCC",
                "TTRCTT",
                "AGCCGG",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.getDiagonalLeft(4,1)).to.be.true;
        });
        it('Diagonal izquierda no encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTCC",
                "TTRCTT",
                "AGACGC",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.getDiagonalLeft(5,3)).to.be.false;
        });
    });
    describe('#getHorizontal()',()=>{
        it('Horizontal encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTCC",
                "TTRCTT",
                "ACCCCG",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.getHorizontal(3,1)).to.be.true;
        });
        it('Horizontal no encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTCC",
                "TTRTTT",
                "AGACGC",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.getHorizontal(2,1)).to.be.false;
        });
    });
    describe('#getVertical()',()=>{
        it('Vertical encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CATTCC",
                "TTTCTT",
                "ACTCCG",
                "GCTTCA",
                "TCACTG"],true);
            expect(mutant.getVertical(1,2)).to.be.true;
        });
        it('Vertical no encontrada',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTCC",
                "TTATTT",
                "AGACGC",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.getVertical(2,2)).to.be.false;
        });
    });
    describe('#isMutant()',()=>{
        it('Es mutante',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTGC",
                "TTATGT",
                "AGAAGG",
                "CCCCTA",
                "TCACTG"],true);
            expect(mutant.isMutant()).to.be.true;
        });
        it('NO es mutante',()=>{
            var mutant = new Mutant([
                "ATGCGA",
                "CAGTGC",
                "TTATTT",
                "AGACGG",
                "GCGTCA",
                "TCACTG"],true);
            expect(mutant.isMutant()).to.be.false;
        });
    });
});